package com.filemanager.findit;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Main Fragment class
 */
public class FileListActivityFragment extends Fragment {

    private ListView mListView;
    private GridView mGridView;

    private String mCurrentPath;
    private boolean onActionMode = false;
    private ArrayAdapter<Item> adapter;
    private SparseBooleanArray itemsToDelete;
    private ActionMode mActionMode;
    private TextView tvEmptyFolder;

    public class Item {
        public String itemName;
        public String itemDate;
        public boolean isFolder;
        public boolean selected;
    }

    private List<Item> mItemList;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // get requested default path
        if (getArguments() != null) {
            mCurrentPath = getArguments().getString("path");
        }
        else {
            mCurrentPath = Environment.getExternalStorageDirectory().toString();
        }
    }

    public FileListActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // to handle change in orientation
        if (savedInstanceState != null) {
            mCurrentPath = savedInstanceState.getString("currentPath");
        }
        return inflater.inflate(R.layout.fragment_file_list, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        tvEmptyFolder = (TextView) getActivity().findViewById(R.id.empty_folder);
        itemsToDelete = new SparseBooleanArray();

        //Run in background
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                populateListView();
                if (!mItemList.isEmpty()) {
                    tvEmptyFolder.setVisibility(View.GONE);
                }else {
                    tvEmptyFolder.setVisibility(View.VISIBLE);
                }

                adapter = new ItemArrayAdapter(getActivity(), mItemList);

                //ListView for portrait mode and GridView for landscape
                if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
                    setupList();
                else
                    setupGrid();
            }
        });
    }

    /**
     * setups the ListView for when device is in portrait
     */
    private void setupList() {
        mListView = (ListView) getActivity().findViewById(R.id.list);
        mListView.setAdapter(adapter);

        mListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("List", "selected " + mItemList.get(position).itemName);
                if (onActionMode) {
                    mItemList.get(position).selected = !mItemList.get(position).selected;
                    itemsToDelete.put(position, mItemList.get(position).selected);
                    adapter.notifyDataSetChanged();
                }
                else {
                    if (mItemList.get(position).isFolder)
                        ((FileListActivity) getActivity()).startNewFragment(mCurrentPath + "/" + mItemList.get(position).itemName);
                    else
                        launchAppPicker(mCurrentPath, mItemList.get(position).itemName);
                }
            }
        });

        mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View view,
                                           int pos, long id) {
                Log.d("long clicked","pos: " + pos);

                if (mActionMode != null) {
                    return false;
                }
                mActionMode = ((AppCompatActivity)getActivity()).startSupportActionMode(mActionModeCallback);

                ((FileListActivity)getActivity()).hideActionBar();
                onActionMode = true;
                mItemList.get(pos).selected = true;
                itemsToDelete.put(pos, true);
                adapter.notifyDataSetChanged();
                return true;
            }
        });
    }

    /**
     * setups the GridView for when device is in landscape
     */
    private void setupGrid() {
        mGridView = (GridView) getActivity().findViewById(R.id.grid);
        mGridView.setAdapter(adapter);

        mGridView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("List", "selected " + mItemList.get(position).itemName);
                if (onActionMode) {
                    mItemList.get(position).selected = !mItemList.get(position).selected;
                    itemsToDelete.put(position, mItemList.get(position).selected);
                    adapter.notifyDataSetChanged();
                }
                else {
                    if (mItemList.get(position).isFolder)
                        ((FileListActivity) getActivity()).startNewFragment(mCurrentPath + "/" + mItemList.get(position).itemName);
                    else
                        launchAppPicker(mCurrentPath, mItemList.get(position).itemName);
                }
            }
        });

        mGridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View view,
                                           int pos, long id) {
                Log.d("long clicked","pos: " + pos);

                if (mActionMode != null) {
                    return false;
                }
                mActionMode = ((AppCompatActivity)getActivity()).startSupportActionMode(mActionModeCallback);

                ((FileListActivity)getActivity()).hideActionBar();
                onActionMode = true;
                mItemList.get(pos).selected = true;
                itemsToDelete.put(pos, true);
                adapter.notifyDataSetChanged();
                return true;
            }
        });
    }

    // listener for operations in ActionMode
    final ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.setTitle("Selected");

            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.menu_delete, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_delete:
                    deleteSelectedItems();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
//                        doneClicked();
            for (int i=0; i<mItemList.size(); i++) {
                mItemList.get(i).selected = false;
            }
            cancelActionMode();
            ((FileListActivity)getActivity()).showActionBar();
        }
    };

    /**
     * saves state for change in orientation
     * @param outState current state
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("currentPath", mCurrentPath);
    }

    /**
     * opens file with available app
     * @param path full directory where file is stored
     * @param filename name of the file
     */
    public void launchAppPicker(String path, String filename) {
        File file = new File(path, filename);

        Uri uri = Uri.fromFile(file);
        String mime = getMimeType(path + "/" + filename);

        Intent intent = new Intent();
        intent.setDataAndType(uri, mime);
        intent.setAction(Intent.ACTION_VIEW);

        try {
            startActivity(intent);
        } catch (android.content.ActivityNotFoundException e) {
            Snackbar.make(tvEmptyFolder, getResources().getString(R.string.empty_targets), Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }
    }

    /**
     * function to get mime type of a certain file
     * @param url full path of the file
     * @return String with type of file
     */
    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }

    /**
     * operations to perform to exit ActionMode
     */
    public void cancelActionMode() {
        onActionMode = false;
        adapter.notifyDataSetChanged();
        mActionMode = null;
    }

    /**
     * creates new instance of the fragment with different path
     * @param path directory of files to visualize
     * @return new fragment to display
     */
    public static FileListActivityFragment newInstance(String path) {
        FileListActivityFragment myFragment = new FileListActivityFragment();

        Bundle args = new Bundle();
        args.putString("path", path);
        myFragment.setArguments(args);

        return myFragment;
    }

    /**
     * refreshes list of visualized files
     */
    public void refreshList() {
        mItemList.clear();
        populateListView();
        adapter = new ItemArrayAdapter(getActivity(), mItemList);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
            mListView.setAdapter(adapter);
        else
            mGridView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        if (!mItemList.isEmpty()) {
            tvEmptyFolder.setVisibility(View.GONE);
        }
        else {
            tvEmptyFolder.setVisibility(View.VISIBLE);
        }
    }

    /**
     * populates the list of items with the files in the mCurrentPath
     */
    private void populateListView() {
        ((FileListActivity)getActivity()).mTvCurrentDirectory.setText(mCurrentPath);

        File f = new File(mCurrentPath);
        File files[] = f.listFiles();

        Arrays.sort(files, new Comparator<File>(){
            public int compare(File f1, File f2)
            {
                if (f1.isDirectory() && !f2.isDirectory())
                    return -1;
                else if (!f1.isDirectory() && f2.isDirectory())
                    return +1;
                else
                    return f1.getName().compareToIgnoreCase(f2.getName());
            }
        });

        mItemList = new ArrayList<>();
        for (File file : files) {
            if (file.isHidden())
                continue;

            Item item = new Item();
            item.itemName = file.getName();
            item.isFolder = file.isDirectory();
            Date lastModDate = new Date(file.lastModified());
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, MMM dd", Locale.ENGLISH);
            item.itemDate = dateFormat.format(lastModDate);
            item.selected = false;

            mItemList.add(item);
        }
    }

    /**
     * deletes the files identified in itemsToDelete
     * TODO: does not work for Lollipop+
     */
    public void deleteSelectedItems() {
        boolean nothingToDelete = true;
        for (int i=0; i<mItemList.size(); i++) {
            if (itemsToDelete.get(i)) {
                nothingToDelete = false;
                break;
            }
        }
        if (nothingToDelete) {
            Snackbar.make(tvEmptyFolder, getResources().getString(R.string.deleted_nothing), Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return;
        }
        new AlertDialog.Builder(getActivity())
                .setTitle(getResources().getString(R.string.deleting_title))
                .setMessage(getResources().getString(R.string.deleting_message))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        boolean deleted = false;
                        for (int i=0; i<mItemList.size(); i++) {
                            if (itemsToDelete.get(i)) {
                                File file = new File(mCurrentPath + "/" + mItemList.get(i).itemName);
                                Log.d("Delete", "Tried to delete " + file.getAbsolutePath());
                                deleted |= file.delete();
                                if(file.exists()){
                                    Log.d("Delete", "File still exists");
                                }
                            }
                        }
                        if (deleted) {
                            Snackbar.make(tvEmptyFolder, getResources().getString(R.string.deleted_success), Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                        }
                        else {
                            Snackbar.make(tvEmptyFolder, getResources().getString(R.string.deleted_error), Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                        }
                        for (int i=0; i<mItemList.size(); i++) {
                            mItemList.get(i).selected = false;
                        }
                        mActionMode.finish();
                        cancelActionMode();
                        ((FileListActivity)getActivity()).showActionBar();
                        refreshList();
                    }
                })
                .setNegativeButton(android.R.string.no, null).show();
    }

    /**
     * adapter for ListView of GridView
     */
    private class ItemArrayAdapter extends ArrayAdapter<Item> {

        private final List<Item> itemList;
        private final Activity activity;

        /**
         * holder with views of each item in the ListView/GridView
         */
        class ViewHolder {
            private TextView tvName;
            private TextView tvDate;
            private ImageView ivIcon;
            private LinearLayout layout;
        }

        public ItemArrayAdapter (Activity activity, List<Item> itemList) {
            super(activity, R.layout.item_layout, itemList);
            this.itemList = itemList;
            this.activity = activity;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;

            if (convertView == null) {
                LayoutInflater inflater = activity.getLayoutInflater();
                view = inflater.inflate(R.layout.item_layout, parent, false);
                final ViewHolder viewHolder = new ViewHolder();
                viewHolder.tvName = (TextView) view.findViewById(R.id.itemName);
                viewHolder.tvDate = (TextView) view.findViewById(R.id.itemDate);
                viewHolder.ivIcon = (ImageView) view.findViewById(R.id.itemIcon);
                viewHolder.layout = (LinearLayout) view.findViewById(R.id.itemLayout);
                view.setTag(viewHolder);
            }
            else {
                view = convertView;
            }

            ViewHolder holder = (ViewHolder) view.getTag();
            holder.tvName.setText(itemList.get(position).itemName);
            holder.tvDate.setText(itemList.get(position).itemDate);
            if (itemList.get(position).isFolder) {
                holder.ivIcon.setImageResource(R.drawable.ic_folder_white_24dp);
                holder.ivIcon.setBackgroundResource(R.drawable.icon_background);
            }
            else {
                holder.ivIcon.setImageResource(R.drawable.ic_insert_drive_file_white_24dp);
                holder.ivIcon.setBackgroundResource(R.drawable.file_background);
            }

            if (itemList.get(position).selected)
                holder.layout.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorSelected));
            else
                holder.layout.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryForeground));

            return view;
        }
    }
}
