package com.filemanager.findit;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.support.design.widget.Snackbar;
import android.view.View;

import java.io.File;

/**
 * Settings activity
 */
public class SettingsActivity extends PreferenceActivity {

    final static String DEFAULT_FOLDER_PREFERENCE_KEY = "default_folder_preference";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Display the fragment as the main content.
        FragmentManager mFragmentManager = getFragmentManager();
        FragmentTransaction mFragmentTransaction = mFragmentManager
                .beginTransaction();
        PrefsFragment mPrefsFragment = new PrefsFragment();
        mFragmentTransaction.replace(android.R.id.content, mPrefsFragment);
        mFragmentTransaction.commit();
    }

    public static class PrefsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            // Load the preferences from an XML resource
            addPreferencesFromResource(R.xml.settings);

            Preference defaultFolderPreference = findPreference(DEFAULT_FOLDER_PREFERENCE_KEY);
            defaultFolderPreference.setDefaultValue(Environment.getExternalStorageDirectory().toString());

            defaultFolderPreference.setSummary(((EditTextPreference) defaultFolderPreference).getText());
        }

        /**
         * To handle refreshing the summary in the preferences
         */
        @Override
        public void onResume() {
            super.onResume();
            getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);

        }

        /**
         * To handle refreshing the summary in the preferences
         */
        @Override
        public void onPause() {
            getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
            super.onPause();
        }

        /**
         * Changes the summary to the selected values
         * @param sharedPreferences Shared preferences of the app
         * @param key Key of the changed preference
         */
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            Preference pref = findPreference(key);

            if (DEFAULT_FOLDER_PREFERENCE_KEY.equals(key)) {
                EditTextPreference editTextPreference = (EditTextPreference) pref;
                pref.setSummary(editTextPreference.getText());

                File dir = new File(editTextPreference.getText());
                if(!dir.isDirectory()) {
                    View view = getView();
                    if (view!=null)
                        Snackbar.make(view, getResources().getString(R.string.default_path_selected_warning), Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                }
            }
        }
    }


}