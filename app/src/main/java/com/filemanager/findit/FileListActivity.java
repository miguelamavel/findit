package com.filemanager.findit;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.File;

/**
 * Main activity
 */
public class FileListActivity extends AppCompatActivity {

    public TextView mTvCurrentDirectory;
//    public AppBarLayout appBarLayout;
    private RelativeLayout appBarLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_list);

        mTvCurrentDirectory = (TextView) findViewById(R.id.current_directory);
        appBarLayout = (RelativeLayout) findViewById(R.id.app_bar_layout);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
            Snackbar.make(mTvCurrentDirectory, getResources().getString(R.string.storage_permission_denied), Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return;
        }

        String defaultFolder = PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsActivity.DEFAULT_FOLDER_PREFERENCE_KEY,
                Environment.getExternalStorageDirectory().toString());

        File dir = new File(defaultFolder);
        if(!dir.isDirectory()) {
            Snackbar.make(mTvCurrentDirectory, getResources().getString(R.string.default_path_failed), Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            defaultFolder = Environment.getExternalStorageDirectory().toString();
        }

        if (savedInstanceState == null) {
            startNewFragment(defaultFolder);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_file_list, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 1) {
            fm.popBackStack();
        } else {
            finish();
        }
    }

    /**
     * function called when refresh button is clicked. It calls another function in the current fragment
     * @param v view clicked
     */
    public void onRefreshClick(View v) {
        Log.d("Menu", "Refresh clicked");
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
            Snackbar.make(mTvCurrentDirectory, getResources().getString(R.string.storage_permission_denied), Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return;
        }
        ((FileListActivityFragment)getSupportFragmentManager().findFragmentById(R.id.fragment)).refreshList();
    }

    /**
     * function called when settings button is clicked. It calls another function in the current fragment
     * @param v view clicked
     */
    public void onSettingsClick(View v) {
        Log.d("Menu", "Settings clicked");
        Intent newIntent = new Intent(this, SettingsActivity.class);
        startActivity(newIntent);
    }

    /**
     * Starts new fragment
     * @param path full directory of the new fragment
     */
    public void startNewFragment(String path) {
        Bundle args = new Bundle();
        args.putString("path", path);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);

        ft.replace(R.id.fragment, FileListActivityFragment.newInstance(path), path)
                .addToBackStack(null)
                .commit();
    }

    /**
     * Hides actionbar
     */
    public void hideActionBar() {
        appBarLayout.setVisibility(View.INVISIBLE);
    }

    /**
     * Shows actionbar
     */
    public void showActionBar() {
        appBarLayout.setVisibility(View.VISIBLE);
    }
}










